<?php

namespace Drupal\opin_views_tailwind\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Component\Utility\Html;
use Drupal\opin_views_tailwind\ViewsTailwind;

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_tailwind_grid",
 *   title = @Translation("Tailwind Grid"),
 *   help = @Translation("Displays rows in a Tailwind Grid layout"),
 *   theme = "opin_views_tailwind_grid",
 *   theme_file = "../opin_views_tailwind.theme.inc",
 *   display_types = {"normal"}
 * )
 */
class ViewsTailwindGrid extends StylePluginBase {
  /**
   * Overrides \Drupal\views\Plugin\views\style\StylePluginBase::usesRowPlugin.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Overrides \Drupal\views\Plugin\views\style\StylePluginBase::usesRowClass.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Definition.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    foreach (ViewsTailwind::getBreakpoints() as $breakpoint) {
      $breakpoint_option = "col_$breakpoint";
      $options[$breakpoint_option] = ['default' => 'none'];
    }
    $options['gap_size_x'] = ['default' => ''];
    $options['gap_size_y'] = ['default' => ''];
    $options['col_class_custom'] = ['default' => ''];
    $options['col_class_default'] = ['default' => TRUE];
    $options['row_class_custom'] = ['default' => ''];
    $options['row_class_default'] = ['default' => TRUE];
    $options['default'] = ['default' => ''];
    $options['info'] = ['default' => []];
    $options['override'] = ['default' => TRUE];
    $options['sticky'] = ['default' => FALSE];
    $options['order'] = ['default' => 'asc'];
    $options['caption'] = ['default' => ''];
    $options['summary'] = ['default' => ''];
    $options['description'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $gap_x_prefix = 'gap-x';
    $form['gap_size_x'] = [
      '#type' => 'select',
      '#title' => $this->t("Pixel gap in the X axis between rows"),
      '#default_value' => isset($this->options['gap_size_x']) ? $this->options['gap_size_x'] : NULL,
      '#options' => [
        $gap_x_prefix . '-0' => 'None',
      ],
    ];
    foreach ([0, 1, 2, 4, 6, 8, 10, 12, 14, 16, 20, 24] as $gap_x) {
      $form['gap_size_x']['#options'][$gap_x_prefix . "-" . $gap_x] = ($gap_x * 4) . 'px gap (X axis)';
    }

    $gap_y_prefix = 'gap-y';
    $form['gap_size_y'] = [
      '#type' => 'select',
      '#title' => $this->t("Pixel gap in the Y axis between rows"),
      '#default_value' => isset($this->options['gap_size_y']) ? $this->options['gap_size_y'] : NULL,
      '#options' => [
        $gap_y_prefix . '-0' => 'None',
      ],
    ];
    foreach ([0, 1, 2, 4, 6, 8, 10, 12, 14, 16, 20, 24] as $gap_y) {
      $form['gap_size_y']['#options'][$gap_y_prefix . "-" . $gap_y] = ($gap_y * 4) . 'px gap (Y axis)';
    }

    foreach (ViewsTailwind::getBreakpoints() as $breakpoint) {
      $breakpoint_option = "col_$breakpoint";
      $prefix = ($breakpoint != 'xs' ? $breakpoint . ':' : '') . 'grid-cols';
      $form[$breakpoint_option] = [
        '#type' => 'select',
        '#title' => $this->t("Column width of items at '$breakpoint' breakpoint"),
        '#default_value' => isset($this->options[$breakpoint_option]) ? $this->options[$breakpoint_option] : NULL,
        '#description' => $this->t("Set the number of columns each item should take up at the '$breakpoint' breakpoint and higher."),
        '#options' => [
          $prefix . '-none' => 'None',
        ],
      ];

      foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] as $col) {
        $form[$breakpoint_option]['#options'][$prefix . "-$col"] = $col . ' per row';
      }
    }
  }

}
