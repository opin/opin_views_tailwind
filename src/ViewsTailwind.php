<?php

namespace Drupal\opin_views_tailwind;

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;

/**
 * The primary class for the Views Tailwind module.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class ViewsTailwind {

  /**
   * Returns the theme hook definition information.
   */
  public static function getThemeHooks() {
    $hooks['views_tailwind_grid'] = [
      'preprocess functions' => [
        'template_preprocess_opin_views_tailwind_grid',
      ],
      'file' => 'opin_views_tailwind.theme.inc',
    ];
    return $hooks;
  }


  /**
   * Return an array of breakpoint names.
   */
  public static function getBreakpoints() {
    return ['xs', 'sm', 'md', 'lg', 'xl'];
  }

  /**
   * Get unique element id.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueId(ViewExecutable $view) {
    $id = $view->storage->id() . '-' . $view->current_display;
    return Html::getUniqueId('views-tailwind-' . $id);
  }

}
