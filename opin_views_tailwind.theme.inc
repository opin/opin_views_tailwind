<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

use Drupal\opin_views_tailwind\ViewsTailwind;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for views grid templates.
 *
 * Default template: opin-views-tailwind-grid.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_opin_views_tailwind_grid(array &$vars) {
  $view = $vars['view'];
  $vars['id'] = ViewsTailwind::getUniqueId($view);
  $vars['attributes']['class'][] = 'grid';
  $options = $view->style_plugin->options;
  $vars['row_attributes'] = new Attribute();

  foreach (ViewsTailwind::getBreakpoints() as $breakpoint) {
    if ($options["col_$breakpoint"] == 'none') {
      continue;
    }
    $vars['row_attributes']->addClass($options["col_$breakpoint"]);
  }

  $vars['row_attributes']->addClass($options["gap_size_x"]);
  $vars['row_attributes']->addClass($options["gap_size_y"]);

  $vars['options'] = $options;
}
